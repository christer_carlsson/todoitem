using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TodoItem.Models;
using System.Linq;
using System.Threading.Tasks;

namespace TodoItem.Controllers
{
    [Route("api/todo")]
    public class TodoController : Controller
    {
        private readonly TodoContext _context;
        public TodoController(TodoContext context)
        {
            _context = context;

            if (_context.TodoItems.Count() == 0)
            {
                _context.Add(new TodoItem.Models.TodoItem { Name = "item1" });
                _context.Add(new TodoItem.Models.TodoItem { Name = "item2" });
                _context.Add(new TodoItem.Models.TodoItem { Name = "item3" });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var items = _context.TodoItems.ToList();
            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetTodo")]
        public IActionResult GetTodoItem(long id)
        {
            var item = _context.TodoItems.SingleOrDefault((td) => td.Id == id);
            if (item is null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TodoItem.Models.TodoItem item)
        {
            if (item == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            _context.TodoItems.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtRoute("GetTodo", new { id = item.Id }, item);
        }
    }
}