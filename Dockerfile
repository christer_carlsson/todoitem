# escape=`
# Build stage
FROM microsoft/aspnetcore-build:2 AS build-env

WORKDIR /todo_build

# Restore
COPY TodoItem/TodoItem.csproj ./TodoItem/
RUN dotnet restore TodoItem/TodoItem.csproj
COPY tests/tests.csproj ./tests/
RUN dotnet restore tests/tests.csproj

# Copy source
COPY . .

# Tests
RUN dotnet test tests/tests.csproj

# Publish
RUN dotnet publish TodoItem/TodoItem.csproj -o /publish

# Runtime stage
FROM microsoft/aspnetcore:2
COPY --from=build-env /publish publish
WORKDIR /publish
ENTRYPOINT [ "dotnet", "TodoItem.dll" ]